// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkateboardingSystemGameMode.generated.h"

UCLASS(minimalapi)
class ASkateboardingSystemGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASkateboardingSystemGameMode();
};



