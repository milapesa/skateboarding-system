﻿#include "SSPlayerState.h"

void ASSPlayerState::AddScore(const FSSScore& ScoreToAdd)
{
	const float OldScore = GetScore();

	// Update score
	FSSScore ScoreWithMultiplier = ScoreToAdd;
	ScoreWithMultiplier.Amount *= CurrentCombo;
	SetScore(OldScore + ScoreWithMultiplier.Amount);
	OnScoreChanged.Broadcast(GetScore(), OldScore, ScoreWithMultiplier);

	// Update combo
	if (ScoreToAdd.Amount > MinScoreToIncreaseCombo)
	{
		CurrentCombo = FMath::Clamp(CurrentCombo + 1, 1, MaxCombo);
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.ClearTimer(ComboTimer);
		TimerManager.SetTimer(ComboTimer, this, &ASSPlayerState::ResetCombo, ComboTime, false);
		OnComboChanged.Broadcast(CurrentCombo, ComboTime);
	}
}

void ASSPlayerState::ResetCombo()
{
	CurrentCombo = 1;
	OnComboChanged.Broadcast(CurrentCombo, 0.0f);
}
