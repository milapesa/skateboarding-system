﻿#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

#include "SSPlayerStuntsComponent.generated.h"

class USSObstacleComponent;

struct FSSScore;

/** Checks for all the stunts the owning character does */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class USSPlayerStuntsComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USSPlayerStuntsComponent();
	
protected:

	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void OwnerMovementModeChange(ACharacter* Character, EMovementMode PrevMovementMode, uint8 PreviousCustomMode);

	EMovementMode GetCharacterOwnerMovementMode() const;

	void AddScore(const FSSScore& Score) const;

	/** Checks the obstacles that the owner character has below */
	void CheckBelowObstacles();

private:

	/** The score gained per second on the air */
	UPROPERTY(EditAnywhere, Category = "SS|OnAir")
	float OnAirScorePerSecond = 10.0f;

	/** Tag to identified the score made by spending time on the air */
	UPROPERTY(EditAnywhere, Category = "SS|OnAir")
	FGameplayTag TimeOnAirTagScore;

	/** How below the obstacles can be to be detected when jumping over */
	UPROPERTY(EditAnywhere, Category = "SS|Obstacle")
	float DistanceObstacleCheck = 500.0f;

	/** Time time between each obstacle check while jumping */
	UPROPERTY(EditAnywhere, Category = "SS|Obstacle")
	float CheckBelowObstacleInterval = 0.1f;

	/** Tag to identified the score made by jumping over obstacles */
	UPROPERTY(EditAnywhere, Category = "SS|Obstacle")
	FGameplayTag JumpOverObstacleTagScore;

	UPROPERTY(EditAnywhere, Category = "SS|Obstacle|Debug")
	bool bDebugObstacleCheck = false;

	/** The current obstacles that we are jumping over */
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>> JumpingOverObstacles;

	/** The obstacles that we are jumped over */
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>> FinishedJumpingOverObstacles;

	FTimerHandle CheckBelowObstacleTimer;
};
