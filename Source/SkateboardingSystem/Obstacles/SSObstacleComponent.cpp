﻿// 


#include "SSObstacleComponent.h"


FText USSObstacleComponent::GetObstacleName() const
{
	return ObstacleName.IsEmpty() ? FText::FromString(GetName()) : ObstacleName;
}
