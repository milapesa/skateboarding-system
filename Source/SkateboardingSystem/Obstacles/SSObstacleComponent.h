﻿// 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SSObstacleComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class USSObstacleComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
	float GetJumpOverScore() const { return JumpOverScore; }

	UFUNCTION(BlueprintPure)
	FText GetObstacleName() const;

private:
	UPROPERTY(EditAnywhere)
	float JumpOverScore = 100.0f;

	UPROPERTY(EditAnywhere)
	FText ObstacleName;
};
