﻿
#include "SSPlayerStuntsComponent.h"

// UE Includes
#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "KismetTraceUtils.h"

// SS Includes
#include "SSObstacleComponent.h"
#include "SSObstacleComponentInterface.h"
#include "SkateboardingSystem/SSPlayerState.h"

USSPlayerStuntsComponent::USSPlayerStuntsComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void USSPlayerStuntsComponent::BeginPlay()
{
	Super::BeginPlay();
	GetOwner<ACharacter>()->MovementModeChangedDelegate.AddDynamic(this, &USSPlayerStuntsComponent::OwnerMovementModeChange);
}

void USSPlayerStuntsComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetOwner<ACharacter>()->MovementModeChangedDelegate.RemoveDynamic(this, &USSPlayerStuntsComponent::OwnerMovementModeChange);
	Super::EndPlay(EndPlayReason);
}

void USSPlayerStuntsComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                           FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetCharacterOwnerMovementMode() == MOVE_Falling)
	{
		AddScore(FSSScore(OnAirScorePerSecond * DeltaTime, TimeOnAirTagScore, GetOwner()));
	}
}

void USSPlayerStuntsComponent::OwnerMovementModeChange(ACharacter* Character, EMovementMode PrevMovementMode,
	uint8 PreviousCustomMode)
{
	const EMovementMode CurrentMovementMode = GetCharacterOwnerMovementMode();
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	if (PrevMovementMode != MOVE_Falling && CurrentMovementMode == MOVE_Falling)
	{
		SetComponentTickEnabled(true);
		
		CheckBelowObstacles();
		TimerManager.SetTimer(CheckBelowObstacleTimer, this, &USSPlayerStuntsComponent::CheckBelowObstacles,
			CheckBelowObstacleInterval, true);
	}
	else
	{
		SetComponentTickEnabled(false);
		
		CheckBelowObstacles();
		TimerManager.ClearTimer(CheckBelowObstacleTimer);
	
		for (AActor* JumpingOverActor : FinishedJumpingOverObstacles)
		{
			const USSObstacleComponent* ObstacleComponent = ISSObstacleComponentInterface::Execute_GetObstacleComponent(JumpingOverActor);
			AddScore(FSSScore(ObstacleComponent->GetJumpOverScore(), JumpOverObstacleTagScore, JumpingOverActor));
		}
		FinishedJumpingOverObstacles.Reset();
		JumpingOverObstacles.Reset();
	}
}

EMovementMode USSPlayerStuntsComponent::GetCharacterOwnerMovementMode() const
{
	return GetOwner<ACharacter>()->GetCharacterMovement()->MovementMode;
}

void USSPlayerStuntsComponent::AddScore(const FSSScore& Score) const
{
	ASSPlayerState* PlayerState = GetOwner<ACharacter>()->GetPlayerState<ASSPlayerState>();
	PlayerState->AddScore(Score);
}

void USSPlayerStuntsComponent::CheckBelowObstacles()
{
	const ACharacter* CharacterOwner = GetOwner<ACharacter>();
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(CharacterOwner);

	TArray<FHitResult> Hits;
	const FVector CharacterLocation = CharacterOwner->GetActorLocation();
	const float CapsuleRadius = CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleRadius();
	const float CapsuleHalfHeight = CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	const FVector SweepEnd = CharacterLocation + FVector::DownVector * DistanceObstacleCheck;
	GetWorld()->SweepMultiByChannel(Hits, CharacterLocation, SweepEnd, FQuat::Identity,
		ECC_Visibility, FCollisionShape::MakeCapsule(CapsuleRadius, CapsuleHalfHeight), Params);

#if !UE_BUILD_SHIPPING
	if (bDebugObstacleCheck)
	{
		DrawDebugSweptSphere(GetWorld(), CharacterLocation, SweepEnd, CapsuleRadius, FColor::Red, false, 0.1f);
	}
#endif // !UE_BUILD_SHIPPING

	TArray<TObjectPtr<AActor>> CurrentObstacles;
	for (const FHitResult& Hit : Hits)
	{
		AActor* Actor = Hit.GetActor();
		if (!Actor->GetClass()->ImplementsInterface(USSObstacleComponentInterface::StaticClass()))
		{
			continue;
		}
		CurrentObstacles.AddUnique(Actor);
	}

	for (AActor* PreviousObstacle : JumpingOverObstacles)
	{
		if (!CurrentObstacles.Contains(PreviousObstacle))
		{
			FinishedJumpingOverObstacles.AddUnique(PreviousObstacle);
		}
	}

	JumpingOverObstacles = CurrentObstacles;
}

