﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "SSObstacleComponentInterface.generated.h"

class USSObstacleComponent;

UINTERFACE(Blueprintable, BlueprintType, MinimalAPI)
class USSObstacleComponentInterface : public UInterface
{
	GENERATED_BODY()
};

/** Gives the functionality to contain a obstacle component */
class ISSObstacleComponentInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	USSObstacleComponent* GetObstacleComponent() const;
};
