// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkateboardingSystemGameMode.h"
#include "SkateboardingSystemCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASkateboardingSystemGameMode::ASkateboardingSystemGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
