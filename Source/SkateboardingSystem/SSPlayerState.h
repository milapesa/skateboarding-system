﻿// 

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "GameFramework/PlayerState.h"
#include "SSPlayerState.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FSSScore
{
	GENERATED_BODY()
	
	FSSScore() {}
	
	FSSScore(float InAmount, FGameplayTag InIdentifierTag, AActor* InInstigator)
		: Amount(InAmount), IdentifierTag(InIdentifierTag), Instigator(InInstigator) {}
	
	UPROPERTY(Transient, BlueprintReadOnly)
	float Amount = 0.0f;

	UPROPERTY(Transient, BlueprintReadOnly)
	FGameplayTag IdentifierTag;

	/** The responsible of giving the score */
	UPROPERTY(Transient, BlueprintReadOnly)
	TObjectPtr<AActor> Instigator;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FSSOnScoreChanged, float, NewScore, float, OldScore, FSSScore, ScoreAdded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSSOnComboChanged, int32, NewCombo, float, RemainingTime);

UCLASS()
class ASSPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	void AddScore(const FSSScore& ScoreToAdd);

	/** Returns the current score multiplier */
	UFUNCTION(BlueprintPure)
	float GetCurrentCombo() const { return CurrentCombo; }

	UPROPERTY(BlueprintAssignable)
	FSSOnScoreChanged OnScoreChanged;

	UPROPERTY(BlueprintAssignable)
	FSSOnComboChanged OnComboChanged;

protected:
	void ResetCombo();

private:
	/** Maximum score multiplier */
	UPROPERTY(EditAnywhere, Category = "SS")
	int32 MaxCombo = 12;

	/** The duration before resetting the combo */
	UPROPERTY(EditAnywhere, Category = "SS")
	float ComboTime = 15.0f;

	/** The minimum amount a score needs to give to increase the combo by one */
	UPROPERTY(EditAnywhere, Category = "SS")
	float MinScoreToIncreaseCombo = 50.0f;

	/** Score multiplier that increases when gaining score */
	int32 CurrentCombo = 1;

	FTimerHandle ComboTimer;
};
