// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkateboardingSystem.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkateboardingSystem, "SkateboardingSystem" );
 