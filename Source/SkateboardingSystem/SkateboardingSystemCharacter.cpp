// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkateboardingSystemCharacter.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// ASkateboardingSystemCharacter

ASkateboardingSystemCharacter::ASkateboardingSystemCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->BrakingDecelerationFalling = 1500.0f;
	TurnRate = 0.01f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)

	SkateMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Skate Mesh"));
	SkateMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	StuntsComponent = CreateDefaultSubobject<USSPlayerStuntsComponent>(TEXT("StuntsComponent"));

	SphereComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Sphere"));
	SphereComponent->SetSimulatePhysics(true);
	SphereComponent->SetupAttachment(RootComponent);
}

FVector ASkateboardingSystemCharacter::ConsumeUserInput()
{
	const FVector Input = FVector(UserInput.X, UserInput.Y, 0.0f);
	UserInput = FVector2d();
	return Input;
}

void ASkateboardingSystemCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void ASkateboardingSystemCharacter::TickActor(float DeltaTime, ELevelTick TickType,
                                              FActorTickFunction& ThisTickFunction)
{
	Super::TickActor(DeltaTime, TickType, ThisTickFunction);

	AlignSkatePosition();

	const FVector RelativeLocation = GetClass()->GetDefaultObject<ASkateboardingSystemCharacter>()->SphereComponent->GetRelativeLocation();
	const FVector TargetLocation = SphereComponent->GetComponentLocation() - RelativeLocation;
	SetActorLocation(FMath::Lerp(GetActorLocation(), TargetLocation, 0.5f));

	const FVector Velocity = SphereComponent->GetPhysicsLinearVelocity();
	const FVector NormalizedVelocity = Velocity.GetSafeNormal();
	if (Velocity.Size() > 5.0f)
	{
		const FVector RightVector = FrontWheelHitNormal.RotateAngleAxis(-90, NormalizedVelocity);
		const FRotator TargetRotation = UKismetMathLibrary::MakeRotationFromAxes(NormalizedVelocity, RightVector, FrontWheelHitNormal);
		SetActorRotation(FMath::Lerp(GetActorRotation(), TargetRotation, 0.1f));
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASkateboardingSystemCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ASkateboardingSystemCharacter::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ASkateboardingSystemCharacter::Look);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void ASkateboardingSystemCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();
	UserInput = MovementVector;

	if (Controller != nullptr)
	{
		const FVector Forward = GetActorForwardVector() * MovementVector.Y;
		const FVector Right = GetActorRightVector() * MovementVector.X;
		SphereComponent->AddForce(Forward * PhysicsAcceleration);
		SphereComponent->AddForce(Right * PhysicsTurnAcceleration);

		if(MovementVector.Y > 0)
		{
			PlaySkateMontage(PushingMontage);
		}
		else
		{
			StopAnimMontage(PushingMontage);
		}
	}
}

void ASkateboardingSystemCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ASkateboardingSystemCharacter::Jump()
{
	SphereComponent->AddForce(FrontWheelHitNormal * GetCharacterMovement()->JumpZVelocity);
}

FHitResult ASkateboardingSystemCharacter::DoLineTraceSingleByChannel(const FVector& Start, const FVector& End,
	bool bShowDebugShape, bool bDrawPersistentShapes) const
{
	FHitResult OutLineTraceResults;
	EDrawDebugTrace::Type DebugTraceType = EDrawDebugTrace::None;
	const ETraceTypeQuery CameraChannel = UEngineTypes::ConvertToTraceType(ECC_Camera);
	
	if(bShowDebugShape)
	{
		DebugTraceType = EDrawDebugTrace::ForOneFrame;

		if(bDrawPersistentShapes)
		{
			DebugTraceType = EDrawDebugTrace::Persistent;
		}
	}

	UKismetSystemLibrary::LineTraceSingle(
		this,
		Start,
		End,
		CameraChannel,
		false,
		TArray<AActor*>(),
		DebugTraceType,
		OutLineTraceResults,
		true);
	
	return OutLineTraceResults;
}

void ASkateboardingSystemCharacter::AlignSkatePosition()
{
	FVector Offset = FVector(0.0f, 0.0f, SkateAlignmentTraceOffset);
	const FVector FrontLocation = SkateMeshComponent->GetSocketLocation("FW");
	const FVector FrontWheelsTraceStart = FrontLocation + Offset;
	const FVector FrontWheelsTraceEnd = FrontLocation - Offset;
	
	const FVector BackLocation = SkateMeshComponent->GetSocketLocation("BW");
	const FVector BackWheelsTraceStart = BackLocation + Offset;
	const FVector BackWheelsTraceEnd = BackLocation - Offset;

	FHitResult FrontWheelsHit = DoLineTraceSingleByChannel(FrontWheelsTraceStart, FrontWheelsTraceEnd, false, false);
	FHitResult BackWheelsHit = DoLineTraceSingleByChannel(BackWheelsTraceStart, BackWheelsTraceEnd, false, false);

	FrontWheelHitNormal = FMath::Lerp(FrontWheelHitNormal, FrontWheelsHit.Normal, 0.1f);

	if (!FrontWheelsHit.bBlockingHit || !BackWheelsHit.bBlockingHit)
	{
		return;
	}

	FRotator WheelsRotation = UKismetMathLibrary::FindLookAtRotation(BackWheelsHit.Location, FrontWheelsHit.Location);
	FRotator SkateRotation = SkateMeshComponent->GetComponentRotation();
	FRotator SkateInterpolation = UKismetMathLibrary::RInterpTo(SkateRotation, WheelsRotation, GetWorld()->GetDeltaSeconds(), 20.f);

	SkateMeshComponent->SetWorldRotation(SkateInterpolation);
}

void ASkateboardingSystemCharacter::PlaySkateMontage(UAnimMontage* MontageToPlay)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(!MontageToPlay) return;
	if(AnimInstance->IsAnyMontagePlaying()) return;

	AnimInstance->Montage_Play(MontageToPlay);
}

