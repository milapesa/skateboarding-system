// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "Obstacles/SSPlayerStuntsComponent.h"
#include "SkateboardingSystemCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UCLASS(config=Game)
class ASkateboardingSystemCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASkateboardingSystemCharacter();

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION(BlueprintCallable)
	FVector ConsumeUserInput();

protected:

	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	virtual void Jump() override;

	virtual void TickActor(float DeltaTime, ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

	FHitResult DoLineTraceSingleByChannel(const FVector& Start, const FVector& End, bool bShowDebugShape = false, bool bDrawPersistentShapes = false) const;

	void AlignSkatePosition();
	
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();

	void PlaySkateMontage(UAnimMontage* MontageToPlay);

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	float PhysicsAcceleration = 100000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	float PhysicsTurnAcceleration = 500000.0f;

	/** Static mesh component */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = SkateMesh, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* SkateMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<USSPlayerStuntsComponent> StuntsComponent;

	/** Static mesh used to move the player with physics */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UStaticMeshComponent> SphereComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SkateProperties)
	float TurnRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Skate Movement", meta = (AllowPrivateAccess = "true"))
	UAnimMontage* PushingMontage;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Skate Movement", meta = (AllowPrivateAccess = "true"))
	float SkateAlignmentTraceOffset = 30.f;

	FVector FrontWheelHitNormal;

	FVector2d UserInput;
	
};

